<?php include 'header.php';?>
        <h1>Program Studi</h1>       
        <a href="formProdi.php" class="btn btn-info btn-sm mb-3 mt-3" >Tambah</a>

        <table class="table">
        <thead class="table-info">
            <tr>
                <th>ID Prodi</th>
                <th>Nama Prodi</th>
                <th>Aksi</th>
            </tr>

        </thead>
        <tbody>
            

            <?php
                $sql = 'SELECT * FROM prodi';
                $query = mysqli_query($conn, $sql);
        
                while ($row = mysqli_fetch_object($query)) {
                    ?>

            <tr>
                <td> <?php echo $row->id_prodi; ?> </td>
                <td><?php echo $row->nama_prodi;?></td>
                <td>
                    <a href="deleteProdi.php?id_prodi=<?php echo $row->id_prodi; ?> "class = "btn btn-danger btn-sm" onclick= "return confirm('Hapus Prodi?');" >Hapus</a>
                </td>
            </tr>

            <?php
                 }

                if (! mysqli_num_rows($query)) {
                    echo '<tr><td colspan="8 class="tect-center">Tidak ada data. </td></tr>';
                }
            ?>
        </tbody>
        </table>

<?php include 'footer.php';?>